package formatter;

import java.util.List;
import java.util.stream.Collectors;

public class NumberFormatter {

    public static List<String> formatNumber(List<String> phoneNumbers) {
        return phoneNumbers.stream()
                .map(s -> s.replaceAll("[^\\d]", ""))
                .collect(Collectors.toList());
    }
}
