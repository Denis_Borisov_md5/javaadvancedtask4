package utilites;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FileUtilites {

    private static final Logger logger = LogManager.getLogger(FileUtilites.class.getName());

    public static List<String> readFile(String readFilePath){

        Path path = Paths.get(readFilePath);
        List<String> stringLines = null;

        try{
            stringLines = Files.readAllLines(path);
        }catch (IOException ex){
            logger.error("Reading error", ex);
        }

        return stringLines;
    }

    public static void writeFile(List<String> dataToFile, String writeFilePath){
        Path path = Paths.get(writeFilePath);
        try {
            Files.write(path, dataToFile);
        } catch (IOException ex) {
            logger.error("Writing file error", ex);
        }
    }
}
