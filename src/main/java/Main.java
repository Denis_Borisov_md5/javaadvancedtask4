import finder.FindNumbers;
import formatter.NumberFormatter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utilites.FileUtilites;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class.getName());
    private static final String INPUT_FILE_PATH = "src/main/resources/files/text.txt";
    private static final String OUTPUT_FILE_PATH = "src/main/resources/files/phone-numbers.txt";

    NumberFormat numberFormat;

    public static void main(String[] args) {
        List<String> lines = FileUtilites.readFile(INPUT_FILE_PATH);
        List<String> phoneNumbers = new ArrayList<>();
        for (String str : lines) {
            phoneNumbers.addAll(NumberFormatter.formatNumber(FindNumbers.find(str)));
        }
        logger.info(phoneNumbers);
        FileUtilites.writeFile(phoneNumbers, OUTPUT_FILE_PATH);
    }
}
